public class HDSetting implements GraphicSetting{
    @Override
    public int getNeededProccessingPower() {
        return 1000;
    }

    @Override
    public void processFrame() {

    }
}
