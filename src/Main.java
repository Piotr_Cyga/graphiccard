public class Main {
    public static void main(String[] args) {
        GraphicCard karta = new GraphicCard();
        karta.setSettings(new HDSetting());
        karta.showSettings();
        karta.setSettings(new MediumSetting());
        karta.showSettings();

    }
}
