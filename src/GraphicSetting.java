public interface GraphicSetting {
    int getNeededProccessingPower();
    void processFrame();
}
