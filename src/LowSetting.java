public class LowSetting implements GraphicSetting{
    @Override
    public int getNeededProccessingPower() {
        return 100;
    }

    @Override
    public void processFrame() {

    }
}
