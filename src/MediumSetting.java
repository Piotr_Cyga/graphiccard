public class MediumSetting implements GraphicSetting {
    @Override
    public int getNeededProccessingPower() {
        return 500;
    }

    @Override
    public void processFrame() {

    }
}
