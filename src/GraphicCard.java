public class GraphicCard {
private GraphicSetting settings;

    public void setSettings(GraphicSetting settings) {
        this.settings = settings;
    }
    public void showSettings() {
        System.out.println("Graphic set to: " + settings.getNeededProccessingPower());
    }
}
